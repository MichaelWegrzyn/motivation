@extends('layouts.app')

@section('content')
<div class="container">
<div class="col-sm-10 col-sm-offset-1">
    @foreach ($quotes as $quote)
        <section class="quote">
            <a class="quote-link" href="/quote/{{$quote->publish_date}}">
                <blockquote>
                    <p>{{$quote->quote}}</p>
                    <p class="author"><i>- {{$quote->author}}</i></p>
                </blockquote>
                <p class="quote_date text-right">Date Published: {{date("F d, Y", strtotime($quote->publish_date))}}</p>
            </a>
        </section>
    @endforeach
    <div class="text-center">
        {{ $quotes->links() }}
    </div>
</div>
<!--<div class="col-xs-4">
    <p>sidebar</p>
</div>-->

</div>    
@endsection