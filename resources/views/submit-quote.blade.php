@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-sm-10 col-sm-offset-1 whiteBG">
         @if (Session::get('message'))
            <div class="alert alert-success">
            {{ Session::get('message') }}
            </div>
        @endif
        <h2>Submit a Quote</h2>
        <p>Fill out the form below to submit a favorite Motivational quote.  Upon successful review the quote will be put in the que for a future, Todays Motivation.</p>
        <form action="/submit-quote" method="post">
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                <label for="email">Email</label>
                <input type="email" name="email" class="form-control email" placeholder="johnsmith@gmail.com">
                @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('quote') ? 'has-error' : ''}}">
                <label for="quote">Quote</label>
                <textarea class="form-control" name="quote" rows="3"></textarea>
                @if ($errors->has('quote'))
                    <span class="text-danger">{{ $errors->first('quote') }}</span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('author') ? 'has-error' : ''}}">
                <label for="author">Author</label>
                <input type="text" name="author" class="form-control author" placeholder="Chuck Norris">
                @if ($errors->has('author'))
                    <span class="text-danger">{{ $errors->first('author') }}</span>
                @endif
            </div>
            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="anon" class="anon" /> Anonymous Author
                    </label>
                </div>
            </div>
            <div class="form-group">
                {!! app('captcha')->display(); !!}
                @if ($errors->has('g-recaptcha-response'))
                    <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                @endif
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" id="submit-quote" class="btn btn-success">Submit Quote</button>
        </form>
    </div>
</div>    
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $('.anon').click(function(){
            if($(this).is(':checked')){
                $('.author').val('Anonymous');
                $('.author').attr('disabled', 'disabled')

            }else{
                $('.author').val('');
                $('.author').removeAttr('disabled');
            }
        });
    });
</script>
@endsection