@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-xs-6 col-xs-offset-3 whiteBG">
        <h3>Admin Panel</h3>
        <div class="list-group">
            <a href="/admin/quotes" class="list-group-item">Quotes</a>
            <a href="/admin/create-quote" class="list-group-item">Create Quote</a>
            <a href="/admin/submitted-quotes" class="list-group-item">Submitted Quotes</a>
        </div>
    </div>
</div>    
@endsection