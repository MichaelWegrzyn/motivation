@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-sm-8 col-sm-offset-2 whiteBG">
         @if (Session::get('message'))
            <div class="alert alert-success">
            {{ Session::get('message') }}
            </div>
        @endif
        <h3>Create Quote</h3>
        <form action="/admin/create-quote" method="post">
            <div class="form-group {{ $errors->has('quote') ? 'has-error' : ''}}">
                <label for="quote">Quote</label>
                <textarea class="form-control" name="quote" rows="3"></textarea>
                @if ($errors->has('quote'))
                    <span class="text-danger">{{ $errors->first('quote') }}</span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('author') ? 'has-error' : ''}}">
                <label for="author">Author</label>
                <input type="text" name="author" class="form-control author" placeholder="Chuck Norris">
                @if ($errors->has('author'))
                    <span class="text-danger">{{ $errors->first('author') }}</span>
                @endif
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" id="submit-quote" class="btn btn-success">Submit Quote</button>
        </form>
    </div>
</div>    
@endsection

@section('scripts')
<script>
</script>
@endsection