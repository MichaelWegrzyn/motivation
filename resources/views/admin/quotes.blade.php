@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-xs-12">
        @if (Session::get('message'))
          <div class="alert alert-success">
            {{ Session::get('message') }}
          </div>
        @endif
        @foreach ($quotes as $quote)
            <section class="admin quote">
                <blockquote>
                    <p>{{$quote->quote}}</p>
                    <p class="author"><i>- {{$quote->author}}</i></p>
                </blockquote>
                <p class="quote_date">Publish Date: {{$quote->publish_date}}</p>
                <br/>
                <div class="btn-group" role="group" aria-label="...">
                    <!--<button type="button" class="btn btn-primary">Edit Quote</button>-->
                    <a href="#" data-href="/admin/quote/{{ $quote->id }}/delete" data-toggle="modal" data-target="#confirm-delete" class="btn btn-danger">Delete Quote</a>
                </div>
            </section>
        @endforeach
    </div>
</div>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="deleteModal">Confirm Delete</h4>
        </div>
        <div class="modal-body">
            <p>Are you sure you want to delete this quote?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <a class="btn btn-danger btn-ok">Delete Quote</a>
        </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {

      $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
      });

    });
  </script>
@endsection