@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-xs-12">
        @if (Session::get('message'))
          <div class="alert alert-success">
            {{ Session::get('message') }}
          </div>
        @endif
        @foreach ($submissions as $quote)
            <section class="admin quote">
                @if ($quote->accepted == 1)
                    <p class="bg-success">Quote has been Accepted</p>
                @endif
                <blockquote>
                    <p>{{$quote->quote}}</p>
                    <p class="author"><i>- {{$quote->author}}</i></p>
                </blockquote>
                <br/>
                <p>Email: {{$quote->email}}<br/>
                Submitted Date: {{date_format($quote->created_at, 'm-d-Y')}}</p>
                @if ($quote->accepted == 0)
                    <form id="form-{{$quote->id}}" action="{{ url('/admin/submitted-quote/accept') }}" method="POST" style="display: none;">
                        <input type="hidden" name="quote" value="{{$quote->quote}}" />
                        <input type="hidden" name="author" value="{{$quote->author}}" />
                        <input type="hidden" name="submission_id" value="{{$quote->id}}" />
                        {{ csrf_field() }}
                    </form>
                    <div class="btn-group" role="group" aria-label="...">
                        <a href="#" class="btn btn-primary" onclick="event.preventDefault(); document.getElementById('form-{{$quote->id}}').submit();">Accept Quote</a>
                        <a href="#" data-href="/admin/submitted-quote/{{ $quote->id }}/delete" data-toggle="modal" data-target="#confirm-delete" class="btn btn-danger">Delete Quote</a>
                    </div>
                @endif
            </section>
        @endforeach
    </div>
</div>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="deleteModal">Confirm Delete</h4>
        </div>
        <div class="modal-body">
            <p>Are you sure you want to delete this quote?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <a class="btn btn-danger btn-ok">Delete Quote</a>
        </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {

      $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
      });

    });
  </script>
@endsection