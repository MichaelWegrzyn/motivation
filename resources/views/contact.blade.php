@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-sm-10  col-sm-offset-1 whiteBG">
        <h2>Contact</h2>
        <p>Please fill out the form below to contact us.  We will resond as soon as possible, but may take up to 72 hours for a response.</p>
        <form>
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" class="form-control" placeholder="John Smith">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" class="form-control" placeholder="johnsmit@gmail.com">
            </div>
            <div class="form-group">
                <label for="subject">Subject</label>
                <input type="text" name="subject" class="form-control" placeholder="You're Awesome">
            </div>
            <div class="form-group">
                <label for="message">Message</label>
                <textarea class="form-control" name="message" rows="3"></textarea>
            </div>
            <div class="form-group">
                {!! app('captcha')->display(); !!}
            </div>
            <button type="submit" class="btn btn-success">Submit</button>
        </form>
    </div>
</div>    
@endsection