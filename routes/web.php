<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// if (!env('ALLOW_REGISTRATION', false)) {
//     Route::any('/register', function() {
//         abort(403);
//         return redirect('/');
//     });
// }

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/quotes', 'QuoteController@index');
Route::get('/quote/{publish_date}', 'QuoteController@single');
Route::get('/submit-quote', 'QuoteSubmissionController@submitQuote');
Route::post('submit-quote', 'QuoteSubmissionController@submitQuotePost');

// Route::get('/contact', 'ContactController@contact');

// Route::get('/about', function(){
//     return view('about');
// });

Route::get('/admin', 'AdminController@index');
Route::get('/admin/quotes', 'AdminController@quotes');
Route::get('/admin/create-quote', 'AdminController@createQuote');
Route::post('/admin/create-quote', 'AdminController@createQuotePost');
Route::get('/admin/submitted-quotes', 'AdminController@submittedQuotes');
Route::get('/admin/submitted-quotes/accepted', 'AdminController@acceptedSubmittedQuotes');

Route::post('/admin/submitted-quote/accept', 'AdminController@acceptSubmission');

Route::get('admin/quote/{id}/delete', 'AdminController@destroyQuote');
Route::get('admin/submitted-quote/{id}/delete', 'AdminController@destroySubmission');
