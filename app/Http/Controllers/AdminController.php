<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Quote;
use App\QuoteSubmission;

use App\Mail\QuoteAccepted;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {       
        return view('admin.index');
    }

    public function quotes(){
        $quotes = Quote::orderBy('publish_date', 'desc')->get();

        return view('admin.quotes', compact('quotes'));
    }

    public function createQuote()
    {       
        return view('admin.create-quote');
    }

    public function createQuotePost(Request $request)
    {       
        $this->validate($request,[
            'quote' => 'required',
        ]);

        $findDate = Quote::orderBy('publish_date', 'desc')->first();
        $publish_date = date('Y-m-d', strtotime($findDate->publish_date. ' + 1 days'));

        $quote = new Quote;
        $quote->quote = $request->quote;
        if($request->author){
            $quote->author = $request->author;
        }else{
            $quote->author = "Anonymous";
        }
        $quote->publish_date = $publish_date;
        //$quote->submission_id = 'NULL';

        $quote->save();

        return redirect('/admin/create-quote')->with('message', 'Quote has been submitted with a publish date of '.$publish_date.'.');
    }

    public function submittedQuotes(){
        $submissions = QuoteSubmission::where('accepted', '=', 0)->orderBy('created_at', 'desc')->get();

        return view('admin.submitted-quotes', compact('submissions'));
    }

    public function acceptedSubmittedQuotes(){
        $submissions = QuoteSubmission::where('accepted', '=', 1)->orderBy('created_at', 'desc')->get();

        return view('admin.submitted-quotes', compact('submissions'));
    }

    public function acceptSubmission(Request $request){

        $findDate = Quote::orderBy('publish_date', 'desc')->first();

        $publish_date = date('Y-m-d', strtotime($findDate->publish_date. ' + 1 days'));

        $quote = new Quote;
        $quote->quote = $request->quote;
        $quote->author = $request->author;
        $quote->publish_date = $publish_date;
        $quote->submission_id = $request->submission_id;
        $quote->save();

        if($quote){
            $submission = QuoteSubmission::where('id', '=', $request->submission_id)->first();
            $submission->accepted = 1;
            $submission->save();

            // \Mail::to($submission->email)->send(new QuoteAccepted);
        }

        return redirect('/admin/submitted-quotes')->with('message', 'User Submitted Quote has been accepted with a publish date of '.$publish_date.'.');
    }

    public function destroyQuote($id){
        $user = Quote::find($id);
        $user->delete();
        return redirect('/admin/quotes')->with('message', 'Quote has been deleted.');
    }

    public function destroySubmission($id){
        $user = QuoteSubmission::find($id);
        $user->delete();
        return redirect('/admin/submitted-quotes')->with('message', 'User Submitted Quote has been deleted.');
    }
}
