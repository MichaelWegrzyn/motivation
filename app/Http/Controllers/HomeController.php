<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Quote;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $single = false;
        $quote = Quote::whereDate('publish_date', '=', date('Y-m-d'))->first();

        if(count($quote) != 1){
            $quote = Quote::inRandomOrder()->first();
        }
        
        return view('home', compact('quote', 'single'));
    }
}
