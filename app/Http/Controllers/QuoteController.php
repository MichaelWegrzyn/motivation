<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Quote;

class QuoteController extends Controller
{
    public function index()
    {
        $today = date('Y-m-d');

        $quotes = Quote::where('publish_date', '<=', $today)->orderBy('publish_date', 'desc')->paginate(10);
        
        return view('quotes', compact('quotes', 'today'));
    }

    public function single($publish_date)
    {
        //Find single quote based on URL
        $single = true;
        $quote = Quote::where('publish_date', '=', $publish_date)->first();
        
        return view('home', compact('quote', 'single'));
    }

}
