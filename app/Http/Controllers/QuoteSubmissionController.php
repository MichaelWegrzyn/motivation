<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\QuoteSubmission;

class QuoteSubmissionController extends Controller
{
    public function submitQuote()
    {
        return view('submit-quote');
    }

    public function submitQuotePost(Request $request)
    {
        $this->validate($request,[
        'email' => 'required|max:255',
        'quote' => 'required',
        'g-recaptcha-response' => 'required|captcha'
        ]);



        $quote = new QuoteSubmission;
        $quote->email = $request->email;
        $quote->quote = $request->quote;

        if($request->author){
            $quote->author = $request->author;
        }else if($request->anon){
            $quote->author = "Anonymous";
        }else{
            $quote->author = "Anonymous";
        }

        $quote->accepted = 0;

        $quote->save();

        return redirect('submit-quote')->with('message', 'Quote has been submitted, Thank you!');
    }
}
