<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteSubmission extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'quote_submissions';

}
